<?php

/**
 * Funcion que genera colores
 * @param int $numero numero de colores a generar
 * @return array los colores solicitados en un array de cadenas
 */

function generaColores($numero){
    $colores=[];
    for($i=0;$i<$numero;$i++){
        $colores[$i]="#";
        for($c=1;$c<7;$c++){
            $colores[$i].= dechex(mt_rand(0, 15));
        }
    }
    return $colores;
}

$salida= generaColores(10);

var_dump($salida);

