<?php

/**
 * Funcion que genera colores
 * @param int $numero numero de colores a generar
 * @return array los colores solicitados en un array de cadenas
 */
$almo=false;

    function generaColores($numero,&$hashTag=true){
        $colores=[];
        for($i=0;$i<$numero;$i++){
            $c=0;//inicializado el contador.
            $limite=6;
            $colores[$i]="";
            if($hashTag){
                $colores[$i]="#";
                $limite=7;
            }
            for(;$c<$limite;$c++){
                $colores[$i].= dechex(mt_rand(0, 15));
            }
        }
        return $colores;
    }

$salida= generaColores(10,$almo);

var_dump($salida);
