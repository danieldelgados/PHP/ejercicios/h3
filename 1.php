<?php
/**
 * La funcion genera una serie de numeros aleatorios y devulve un array con los numeros ordenados de forma ascendente
 * La variable $salida es igual a lo que devulve la funcion, es ahí donde llamamos a la funcion e ingresamos los valores.
 * $vMin este será el valor minimo
 * $vMax este será el valor máximo
 * &nValores numero de valores a generar 
 */

 
$salida=ejercicio1(1,10,10);



function ejercicio1($vMin,$vMax,$nValores){
  $salida=[];
  
  for($c=0;$c<$nValores;$c++){
      $salida[$c]= mt_rand($vMin, $vMax);
  }
    sort($salida);// asi ordenamos el array de forma ascendente
    return $salida;// aqui devolvemos un array con numeros aleatorios ordenado de forma ascendente
};

var_dump($salida);